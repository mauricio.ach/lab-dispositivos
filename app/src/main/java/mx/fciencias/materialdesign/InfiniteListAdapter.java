package mx.fciencias.materialdesign;

import android.content.res.Resources;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.LinkedList;
import java.util.List;

public class InfiniteListAdapter extends RecyclerView.Adapter<InfiniteListAdapter.ListEntry> {

    private final List<String> DATASET;
    private final Resources RESOURCES;

    public InfiniteListAdapter(Resources res) {
        DATASET = new LinkedList<>();
        RESOURCES = res;
    }

    @NonNull
    @Override
    public ListEntry onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        TextView tv = (TextView) LayoutInflater.from(parent.getContext()).inflate(R.layout.infinite_list_entry,
                parent,false);
        return new ListEntry(tv);
    }

    @Override
    public void onBindViewHolder(@NonNull ListEntry holder, int position) {
        holder.entryText.setText(DATASET.get(position));
    }

    @Override
    public int getItemCount() {
        return DATASET.size();
    }

    public void addItem(){
        int i = DATASET.size();
        DATASET.add(i,RESOURCES.getString(R.string.infinite_list_entry_message, i+1));
        notifyItemInserted(i);
        Log.i(InfiniteListAdapter.class.getSimpleName(), "Going");
    }

    static class ListEntry extends RecyclerView.ViewHolder {

        private TextView entryText;

        public ListEntry(TextView entryTV) {
            super(entryTV);
            entryText = entryTV;
        }
    }
}
