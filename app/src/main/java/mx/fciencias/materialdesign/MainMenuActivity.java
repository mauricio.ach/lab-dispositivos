package mx.fciencias.materialdesign;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AlertDialog;

import android.content.DialogInterface;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;


public abstract class MainMenuActivity extends AppCompatActivity {
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }


    public static final byte RESULT_EXIT = 2;
    @Override
    public boolean onOptionsItemSelected( @NonNull MenuItem item) {
        switch(item.getItemId()) {
            case R.id.menu_about :
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                alertDialogBuilder.setTitle(R.string.menu_about)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setMessage(R.string.about)
                        .setPositiveButton(R.string.ok,new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        })
                        .create() .show();
                return true;
                case R.id.menu_close_app :
                setResult(RESULT_EXIT);
                finish();
                return true;
            default:
                return
                        super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(resultCode == RESULT_EXIT) {
            setResult(RESULT_EXIT);
            finish();
        } else super.onActivityResult(requestCode, resultCode, data);
    }
}


