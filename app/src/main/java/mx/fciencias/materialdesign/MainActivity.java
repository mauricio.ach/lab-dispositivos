package mx.fciencias.materialdesign;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends MainMenuActivity {

    public static final String VALOR_ID = "mx.fciencias.materialdesign.anon";
    private Button launchInfiniteListButton;
    private String sharedViewTransitionName;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void algo(View openButton){
        Intent starter = new Intent(this, SecondActivity.class);
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            startActivityForResult(starter, RESULT_EXIT);
        } else {
            if(launchInfiniteListButton == null)
                launchInfiniteListButton = findViewById(R.id.launch_infinite_list_button);
            if(sharedViewTransitionName == null)
                sharedViewTransitionName = getString(R.string.shared_button_transitionName);
            ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(this,launchInfiniteListButton,sharedViewTransitionName);
            startActivityForResult(starter,RESULT_EXIT,options.toBundle());
        }
    }
}
